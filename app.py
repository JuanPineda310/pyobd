import asyncio
import datetime
import random
import websockets
import json

async def time(websocket, path):
	speed = 1
	RPM = 100
	while True:
		UtcTime = datetime.datetime.utcnow().isoformat()+"Z"
		if speed < 200:
			speed = speed + 1
		if RPM < 9000:
			RPM = RPM + 10
		ObdData = json.dumps({'speed': speed,'RPM': RPM, 'time': UtcTime})
		await websocket.send(ObdData)
		await asyncio.sleep(0.08)

start_server = websockets.serve(time, "127.0.0.1", 5000)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()